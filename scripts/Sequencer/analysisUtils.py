#!/usr/bin/env python3

#
#  Copyright (C) 2019-2023 CERN for the benefit of the FASER collaboration
#


import gzip
import json
import glob
import requests
import sys

_runInfoCache={}

def getRunInfo(run):
    global _runInfoCache
    if not run in _runInfoCache:
        r = requests.get(f'http://faser-runnumber.web.cern.ch/RunInfo/{run}')
        if r.status_code==200:
            _runInfoCache[run]=r.json()
        else:
            raise Exception(f"Failed to get info on run {run}")
    return _runInfoCache[run]


def getSequence(seqnumber):
    r = requests.get(f'http://faser-runnumber.web.cern.ch/SeqList')
    if r.status_code==200:
        seqinfo=r.json()
    else:
        raise Exception("Failed to get sequence list")
        
    for seq in seqinfo:
        if seq["runnumber"]==seqnumber:
            runs=seq["runs"]
            goodRuns={}
            for run in sorted(runs):
                info=getRunInfo(run)
                seqStep=info['seqstep'] #ignoring substeps for now
                goodRuns[seqStep]=run
            return sorted(goodRuns.values())
    raise Exception(f"Failed to find sequence {seqnumber}")

def getHistograms(run,area="/data"):
    hists=glob.glob(area+f"/Faser-Histograms-{run:06d}-*gz")
    if not hists:
        raise Exception(f"No histograms saved for run {run}")
    histName=sorted(hists)[-1]
    hists=json.load(gzip.open(histName))
    return hists

def histMean(hist):
    if hist['type']!='num_fixedwidth':
        raise Exception(f"Cannot find mean of '{hist['name']}' as it is of type '{hist['type']}'")
    if len(hist['yvalues'])!=hist['xbins']+2:
        raise Exception(f"No overflow bins in  '{hist['name']}' - I am confused")
    delta=(hist['xmax']-hist['xmin'])/hist['xbins']
    weightedSum=0
    totalSum=0
    x0=hist['xmin']-0.5*delta
    yvalues=hist['yvalues']
    for ii in range(1,hist['xbins']+1):
        y=yvalues[ii]
        totalSum+=y
        weightedSum+=y*(x0+ii*delta)
    if totalSum>0:
        return weightedSum/totalSum
    return 0

def histAbove(hist,threshold):
    if hist['type']!='num_fixedwidth':
        raise Exception(f"Cannot find events above threshold of '{hist['name']}' as it is of type '{hist['type']}'")
    if len(hist['yvalues'])!=hist['xbins']+2:
        raise Exception(f"No overflow bins in  '{hist['name']}' - I am confused")
    delta=(hist['xmax']-hist['xmin'])/hist['xbins']
    totalSum=0
    x0=hist['xmin']-0.5*delta
    yvalues=hist['yvalues']
    for ii in range(int((threshold-x0)/delta),hist['xbins']+1):
        y=yvalues[ii]
        totalSum+=y
    return totalSum


if __name__ == '__main__':
    hists=getHistograms(int(sys.argv[1]))
    print(hists['digitizermonitor01'].keys())
    for ch in range(4,15):
        hist=hists['digitizermonitor01'][f'h_h_peak_ch{ch:02d}']
        print(ch,histAbove(hist,1000))
